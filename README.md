# Slope Session - UIStackView Magic: Animating UIStackViews #

[![stackviewmagic.png](https://bitbucket.org/repo/9LeroX/images/503259264-stackviewmagic.png)](https://youtu.be/ZRwZ3udgkFk)
*Click the image above to watch this Slope Session on YouTube.*

### Description ###

This is the source code accompanying the UIStackView Magic Slope Session. The UI contains a UIStackView and utilises @IBOutlet Collections and the 'forEach' method to iterate over a collection of views in a UIStackView to hide/show them intelligently.

Happy coding!

![170.png](https://bitbucket.org/repo/9LeroX/images/1216100977-170.png)

**Caleb Stultz**